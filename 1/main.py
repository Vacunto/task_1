import random
import string
import sys

def generate_random_email(name_length):
    name = ''.join(random.choices(string.ascii_letters, k=name_length))
    domain = ''.join(random.choices(string.ascii_lowercase, k=5))
    extension = random.choice(['com', 'net', 'org', 'edu'])
    return f"{name}@{domain}.{extension}"

if __name__ == "__main__":
    name_length = int(sys.argv[1])
    random_email = generate_random_email(name_length)
    print(random_email)