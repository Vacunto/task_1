CREATE TABLE students (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    date_of_birth DATE
);

CREATE TABLE exams (
    id SERIAL PRIMARY KEY,
    student_id INT,
    subject VARCHAR(100),
    exam_date DATE,
    teacher_name VARCHAR(100),
    grade INT, 
    FOREIGN KEY (student_id) REFERENCES students(id)
);

INSERT INTO students (first_name, last_name, date_of_birth)
VALUES
    ('Иван', 'Иванов', '1995-03-15'),
    ('Петр', 'Петров', '1996-05-20'),
    ('Мария', 'Сидорова', '1997-11-10'),
    ('Елена', 'Козлова', '1998-07-25'),
    ('Андрей', 'Смирнов', '1999-01-30');

INSERT INTO exams (student_id, subject, exam_date, teacher_name, grade)
VALUES
    (1, 'Математика', '2023-06-10', 'Иванова А.Б.', 4),
    (1, 'Физика', '2023-07-15', 'Петров В.Г.', 3),
    (2, 'История', '2023-06-20', 'Сидорова Е.Д.', 4),
    (3, 'Литература', '2023-08-05', 'Козлов К.Л.', 5),
    (4, 'Химия', '2023-07-25', 'Соколова О.Н.', 4),
    (5, 'География', '2023-08-15', 'Николаев Н.М.', 3),
    (1, 'Биология', '2023-09-10', 'Иванова А.Б.', 4),
    (3, 'Иностранный язык', '2023-09-20', 'Антонова М.С.', 5),
    (2, 'Экономика', '2023-10-05', 'Петров П.П.', 3),
    (4, 'Информатика', '2023-10-20', 'Козлова Е.В.', 4);