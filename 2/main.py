import psycopg2
import time

time.sleep(20)

conn = psycopg2.connect(
    dbname="root",
    user="root",
    password="root",
    host="172.19.0.2",
    port="5432"
)

cur = conn.cursor()

cur.execute("""
    SELECT s.first_name, s.last_name, AVG(e.grade) AS average_grade
    FROM students s
    JOIN exams e ON s.id = e.student_id
    GROUP BY s.id
    ORDER BY average_grade
    LIMIT 1;
""")
rows = cur.fetchall()

with open('result.txt', 'w') as file:
    file.write("ФИО\t\tСредний балл\n")
    for row in rows:
        file.write(f"{row[0]} {row[1]}\t{row[2]}\n")

cur.close()
conn.close()